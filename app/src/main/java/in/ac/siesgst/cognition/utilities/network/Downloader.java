package in.ac.siesgst.cognition.utilities.network;


import android.app.Application;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Srinath on 08-07-2015.
 */
public class Downloader extends Application {
    static Downloader singletonObject;
    RequestQueue networkRequestQueue;

    private Downloader() {
        networkRequestQueue = Volley.newRequestQueue(getApplicationContext());
    }

    public static synchronized Downloader getDownloaderInstance() {
        if (singletonObject == null) {
            singletonObject = new Downloader();
        }
        return singletonObject;
    }

    public void downloadJsonData(final Object callerInstance, String URL, final HashMap postData) {
        JsonObjectRequest networkRequest = new JsonObjectRequest(Request.Method.POST, URL, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ((registerJSONCallback) callerInstance).onJSONDownloadedCallback(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ((registerJSONCallback) callerInstance).onJSONErrorCallback(error.getMessage());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //TODO add post parameters
                /*for(Object data:postData){
                    String postValue=(String)data;
                    dataMap.put(KEY_VALUE[],postValue);
                }*/
                return postData;
            }
        };
        networkRequestQueue.add(networkRequest);
    }

    public interface registerJSONCallback {
        void onJSONDownloadedCallback(JSONObject jsonObject);

        void onJSONErrorCallback(String errorMessage);
    }
}


