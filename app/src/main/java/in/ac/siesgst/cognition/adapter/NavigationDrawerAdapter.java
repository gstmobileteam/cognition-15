package in.ac.siesgst.cognition.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import in.ac.siesgst.cognition.R;

/**
 * Created by Srinath on 19-07-2015.
 */
public class NavigationDrawerAdapter extends BaseAdapter {
    private String[] navDrawerItems;
    private String[] navDrawerItemsSubText;
    Context context;
    public NavigationDrawerAdapter(Context context, String[] navDrawerItems,String[] navDrawerItemsSubText){
        this.navDrawerItems=navDrawerItems;
        this.context=context;
        this.navDrawerItemsSubText=navDrawerItemsSubText;
    }
    public NavigationDrawerAdapter(Context context, String[] navDrawerItems){
        this.navDrawerItems=navDrawerItems;
        this.context=context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null)
        {
            convertView= LayoutInflater.from(context).inflate(R.layout.adapter_drawer_list,parent,false);
        }
        TextView navDrawerItem=(TextView)convertView.findViewById(R.id.navigation_drawer_event_alias);
        //TextView subText=(TextView)convertView.findViewById(R.id.navigation_drawer_event_type);
        //subText.setText(navDrawerItemsSubText[position]);
        navDrawerItem.setText(navDrawerItems[position]);
        //navDrawerItem.setTypeface(Typeface.createFromAsset(context.getAssets(),"eminenz.ttf"));
        return convertView;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public String getItem(int position) {
        return navDrawerItems[position];
    }

    @Override
    public int getCount() {
        return navDrawerItems.length;
    }
}
